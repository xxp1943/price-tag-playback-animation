#ifndef __FONT_H__
#define __FONT_H__

#define Font16x16_YEAR      7
#define Font16x16_MONTH     8
#define Font16x16_DAY      0
#define Font16x16_WEEK      9

extern unsigned char const nAsciiDot5x8[][5];
extern unsigned char const Font24x48[][144];
extern unsigned char const Font42x56[][294];
extern unsigned char const Font10x56[][70];
extern unsigned char const Font16x16[][32];
extern unsigned char const Font8x16[][16];



#define FLASH_FONT_VERSION          0x0001

#define FLASH_FONT_16X8_CNT         2
#define FLASH_FONT_36X48_CNT        10
#define FLASH_FONT_16X48_CNT        1
#define FLASH_FONT_8X16_CNT         0x5F
#define FLASH_FONT_16X16_CNT        1

#define FLASH_FONT_BASE_ADDR        (0x0UL)

typedef struct 
{
    uint16_t ver;
    uint16_t _ver;

    unsigned char font16x8[FLASH_FONT_16X8_CNT][16];
    unsigned char font36x48[FLASH_FONT_36X48_CNT][216];
    unsigned char font16x48[FLASH_FONT_16X48_CNT][96];
    unsigned char font8x16[FLASH_FONT_8X16_CNT][16];
    unsigned char font16x16[FLASH_FONT_16X16_CNT][32];

}t_FLASH_FONT;

#define FLASH_FONT                  ((cosnt t_FLASH_FONT *)(FLASH_FONT_BASE_ADDR))

#endif
