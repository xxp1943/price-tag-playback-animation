压缩字模bin文件

对连续相同的字符进行压缩。压缩后的格式：Key字节 + 数据字节 + 长度字节。
其中长度字节N表示有N+1个数据。

命令行格式：
simple_compressor.exe <Input Bin File> <Output Bin File>
